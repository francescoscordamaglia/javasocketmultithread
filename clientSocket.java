
package testClient;

import java.net.*;
import java.io.*;

import java.util.Random;


/**
 * This class implements java socket client
 * @author Francesco Scordamaglia
 *
 */

public class clientSocket {

    public static void main(String[] args) throws Exception{
        
    	try{
       
        ObjectOutputStream oos = null;
        ObjectInputStream ois = null;
        int limit = 100;
        int i = 0;
        int status = 1;
        
        //establish socket connection to server
        Socket socket = new Socket("127.0.0.1", 9876);
      
        Random r = new Random();
        int nomeClient = r.nextInt(10);
        while(status == 1) {
        	
        	
        	//write to socket using ObjectOutputStream
        	oos = new ObjectOutputStream(socket.getOutputStream());
        	
        	if( i == limit ) {
        		
        		
        		
        		System.out.println("Client " + nomeClient + " Sending request to Socket Server: exit ");
            	oos.writeObject("exit");
            	
            	ois = new ObjectInputStream(socket.getInputStream());
            	String message = (String) ois.readObject();
                System.out.println("Response from server: " + message);
                
                oos.close();
                ois.close();
                socket.close();
                
                status = 0;
                continue;
        		
        		
        	}
        	
        	
        	
        	
        	String richiesta = "Client 1 richiesta n° " + i;
        	System.out.println("Sending request to Socket Server: " + richiesta);
        	oos.writeObject(richiesta);
        	
        	
        	//read the server response message
        	ois = new ObjectInputStream(socket.getInputStream());
        	String message = (String) ois.readObject();
            System.out.println("Response from server: " + message);
            
           
            i++;
            
        	
        	
        }
        
        
        System.out.println("End");
        
       
    
    	
    }catch(Exception e){
        System.out.println(e);
    }
    	
    	
    }
    
    
    
}


