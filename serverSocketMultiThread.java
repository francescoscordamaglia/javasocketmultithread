package test;


import java.net.*;

import test.serverThread;
/**
 * This class implements java Socket server
 * @author Francesco Scordamaglia
 *
 */
public class serverSocketMultiThread {
    
    
    
    
    
    public static void main(String args[])  throws Exception{
        //create the socket server object
    	ServerSocket server = new ServerSocket(9876);
       
        //keep listens indefinitely until receives 'exit' call or program terminates
        int count = 0;
        while(true){
        	
        	count++;
        	System.out.println("Numero cicli " + count);
            System.out.println("[0] Waiting for the client request ");
            //creating socket and waiting for client connection
            Socket socket = server.accept();
            
            System.out.println("[1] Recived request from client " + count);
            
            // After connection handle clients in threads
            serverThread newThread = new serverThread(socket);
            newThread.start();
            
            // stampo il numero di thread creato
            System.out.println("[2] Created " + newThread);
            
            System.out.println("[3] Thread ID: "  + newThread.getId());
            
            Thread currThread = Thread.currentThread();
            
            ThreadGroup  test = currThread.getThreadGroup();
            
            test.list();
            
           
            if( count == -1 ) break;
            
        }
        
        System.out.println("Shutting down Socket server!!");
        //close the ServerSocket object
        server.close();
    }
    
}


